package com.example.testapp.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class Diagram constructor(context: Context) : View(context) {
    private var pxPerUnit: Int = 0
    private var padding: Int = 0
    private var zeroX: Int = 0
    private var zeroY: Int = 0
    private var markers: List<Marker> = getDemoMarkers()

    constructor(context: Context, attr: AttributeSet) : this(context) {

    }

    constructor(context: Context, attr: AttributeSet, defStyleAttr: Int) : this(context) {

    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
    }

    override fun onDraw(canvas: Canvas) {
        calcPositions()
        drawLineAndMarkers(canvas)
        super.onDraw(canvas)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
    }


    private fun calcPositions() {
        val max = markers.maxBy { it.value }?.value ?: 0
        val min = markers.minBy { it.value }?.value ?: 0
        pxPerUnit = 600 / (max.minus(min))
        zeroY = max * pxPerUnit + paddingTop

        val step = (width - 2 * padding - 0) / (markers.size - 1)
        for ((i, marker) in markers.withIndex()) {
            val x = step * i + paddingLeft
            val y = zeroY - marker.value * pxPerUnit
            marker.currentPosX = x
            marker.currentPosY = y
        }
    }

    private fun drawLineAndMarkers(canvas: Canvas) {
        var previousMarker: Marker? = null
        val paint = Paint()
        paint.color = Color.GREEN
        paint.strokeWidth = 4.0f
        for (marker in markers) {
            if (previousMarker != null) {
                // draw the line
                val p1X = previousMarker.currentPosX.toFloat()
                val p1Y = previousMarker.currentPosY.toFloat()
                val p2X = marker.currentPosX.toFloat()
                val p2Y = marker.currentPosY.toFloat()

                canvas.drawLine(p1X, p1Y, p2X, p2Y, paint)
            }
            previousMarker = marker
            canvas.drawCircle(
                marker.currentPosX.toFloat(),
                marker.currentPosY.toFloat(),
                7.0f,
                paint
            )
        }
    }

    private fun getDemoMarkers(): List<Marker> {
        val newMarkersList = mutableListOf<Marker>()
        newMarkersList.add(Marker(0))
        newMarkersList.add(Marker(197))
        newMarkersList.add(Marker(184))
        newMarkersList.add(Marker(143))
        newMarkersList.add(Marker(456))
        newMarkersList.add(Marker(487))
        newMarkersList.add(Marker(320))
        newMarkersList.add(Marker(28))
        newMarkersList.add(Marker(68))
        newMarkersList.add(Marker(250))
        newMarkersList.add(Marker(310))
        newMarkersList.add(Marker(563))
        newMarkersList.add(Marker(172))
        newMarkersList.add(Marker(231))
        newMarkersList.add(Marker(56))
        newMarkersList.add(Marker(111))
        newMarkersList.add(Marker(278))
        newMarkersList.add(Marker(391))
        newMarkersList.add(Marker(516))
        return newMarkersList
    }
}

class Marker(val value: Int) {
    var currentPosX = 0
    var currentPosY = 0
}